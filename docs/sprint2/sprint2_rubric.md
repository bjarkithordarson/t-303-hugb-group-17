# Sprint 2 Description
(Version 1)

## 1. Deadlines
-  25th September for H1 and H3
-  26th September for H2, H4 and Akureyri
-  27th September for HMV

All deadlines are 23:59 UTC.

## 2. Activities at a Glance
1. **Plan** your sprint. Details are covered in Section 3.
	1. Randomly choose a Scrum Master
	2. Estimate and choose user stories for Sprint 2
	3. Agree on a definition of done for the sprint
	4. Break down user stories into tasks and divide among team members
2. **Start implementing**. Details are covered in Section 4.
	1. Implement the tasks/user stories for Sprint 2
	2. Provide automated unit tests with at least 50% statement coverage
	3. Comment your code with docstrings.
	4. Leave instructions how to run your application.
3. **Maintain** the decision protocol. See Section 5.

## 3. Sprint Planning
You will start Sprint 2 with a sprint planning session. The tasks you need to do are discussed in the subsections.

### Choose Scrum Master
Before or during your meeting with the TA, **choose a random group member** as a Scrum Master for this sprint. From now on, you will do this in each sprint, every time choosing someone who has not yet been Scrum Master. 

### Estimate User Stories and Populate Sprint Backlog
To plan Sprint 2, try to **decide which user stories you need/want to implement**. 
To do so, you first discuss what is most important to do. Then, you estimate how much time each user story takes. To do so, it is a good practice that each group member comes up with an estimate themselves, and then the estimates are discussed in the group. You can also use any of the variants described online, e.g., on <https://en.wikipedia.org/wiki/Planning_poker>. Similarly, there are a variety of tools available that you can use. Once the user stories have time or point estimates, discuss in the group how much work you think you can do in a sprint. Move the selected user story issues in GitLab to your sprint backlog. To do so, create an additional issue list/label on GitLab for the sprint backlog.

### Agree on a Definition of Done (DoD)
You need to **agree with the TA** what it means that you have completed the sprint, the so-called definition of done or DoD. What will you demonstrate? A list of running test cases? A scenario using a command-line interface? Anything else? Add the DoD as an issue to your sprint backlog, so that the TA can have a look at it.

### Break down User Stories for Sprint 2
Once you agreed on the sprint backlog, **break down all user stories for Sprint 2** into tasks that can be handled by individuals. All group members then pick a number of tasks that they think they can complete - tasks are not assigned. The amount and complexity of tasks can vary between group members based on their skill level. However, everyone has to do some implementation and testing. The picked tasks and commits on GitLab are the individual contribution records for each group member. 

## 4. During the Sprint
During Sprint 2, you will start implementing the application that you have proposed, based on your sprint planning. This means that you start implementing, in Python, the functionality in your sprint backlog. 

### Implement the Application
Implement your tasks/user stories according to your sprint planning. Make sure you support each other and communicate, not work in complete isolation from the others. The technical considerations for the application are further defined below, in Section 6.

### Provide Automated Tests
For testing, your code needs to have at least 50% statement coverage through unit tests. The test folders, virtual environment, or any other system folders are excluded from this number. How to write the tests and calculate coverage will be covered in detail in the lecture on quality assurance.

### Comment your Code
In addition to testing, **comment your code** in a proper way. For documentation, make sure that you comment all of your code in a way that makes it understandable. Try to document high-level constructs (e.g., what does a function do?) and non-obvious code parts. Use standard Python docstrings to provide comments to classes, modules, and functions. For details on docstrings, see for example <https://www.python.org/dev/peps/pep-0257/>. 

### Provide Instructions on Running the Application
To avoid technical issues, **clearly document how your application is run**. To do so, add a file called README (or README.md if you like) to the root folder of your repository that contains the information necessary to run the application and the test code. You are allowed to add further documentation in this file if you like.

## 5. Maintain the Decision Protocol
**Keep maintaining the decision protocol** that you have started during Sprint 1.


## 6. Technical Design Considerations
The project overview document already outlined some constraints on the programming environment and libraries to use.
Beyond the libraries, we require a specific way of writing the application.
That is, you have to provide your application as one or several standalone Python application(s) that communicate with the outside world through gRPC.

gRPC is an implementation of so-called remote procedure calls (RPCs) originally provided by Google.
RPCs are a way to send messages between standalone applications or components - basically a way to call methods.
For simplicity, think of an alternative to HTTP calls.
gRPC is well-documented on <https://grpc.io>.
We will also provide a sample tutorial in Python towards the end of this week.

gRPC is a popular way of communicating between smaller services and components, where HTTP is not always ideal.
We introduce it in this project primarily to show you alternatives to HTTP.

To demo the application, you might have to provide a sample client that makes use of the respective gRPCs. It does not have to be a client that allows for full flexibility, but can provide sample calls only (e.g., with specific parameter values).

## 7. Submission and Assessment
Submission and assessment works as in Sprint 1. Do not forget the peer evaluation.
Please carefully read the rubric that is published together with this assignment.