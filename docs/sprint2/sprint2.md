# Meeting Notes

## Meeting Date: 2023/9/12

**Present**: @SpicyBeard, @LampCat, @hilmir21, @bjarkithordarson

**Absent**: @dadimarpatti

**Supervisor**: @SpicyBeard

**Secretary**: @bjarkithordarson

### Meeting Progression

- Defined the criteria for "done" during this sprint.
- Discussed starting coding and shared a plan for it.
- Considered creating a more elaborate class diagram.
- Discussed the API acquisition from TA or Grischa.
- Randomly selected Daði Már as the first Scrum Master.
- Discussed the use of gRPC for API requests.
- Decided not to use third-party libraries initially.
- Mentioned the README requirements and system running instructions.
- Decided not to elaborate on the database and use a dummy database.
- Discussed using gRPC for API requests.
- Agreed on meeting next Friday at 14:30.

### Decisions

- "Done" criteria: Acquire API and documentation, create module for user/tickets, more detailed class diagram, setup class snippets, function snippets.
- Daði Már is the first Scrum Master, chosen randomly.
- The order of Scrum Master selection: Aron, Bjarki, Daði R, Daði M, Hilmir, determined by a random number generator.

### For the Next Meeting (2023/9/14)

- Elicit requirements from TA.
- Explore GenderMag and BrightSparks.
- Aron Secretary 
- Bjarki Supervisor


## Meeting 2023/9/14
**Present**: @dadimarpatti, @SpicyBeard, @LampCat, @hilmir21, @bjarkithordarson

**Supervisor**: @bjarkithordarson

**Secretary**: @LampCat

### Meeting Progression
- Started by identifying the user stories/issues we are going to use.
- Discussed the use of a mockDB.
- Defined the "Definition of Done" for sprint 2.
- Discussed the sprint backlog.
- Created issues for sprint 2.
- Decided on who is going to work on the skeleton and class diagram
### Decisions
- @bjarkithordarson  will create a class diagram for the ticket and user.
- @dadimarpatti was randomly selected as the Scrum Master.
- @LampCat will start the skeleton for the project


## Meeting Date: 2023/09/19
Present: @SpicyBeard, @dadimarpatti, @LampCat, @bjarkithordarson, @hilmir21
Supervisor: @SpicyBeard
Secretary: @bjarkithordarson

## Meeting Progression
- TA notes: Missing requirements in report. Requirements are in the SPRINT1.md file - should contain a little bit more details. The group should add tasks for the sprint. Tasks are inside issues. Continuous progress passed. Read rubrik carefully.
- Aron found Strætó API documentation. Sent to the Discord server.
- The design of ClapIT will be 3-tiered.
- Daði R will add details to requirements.
- Aron will add tasks to issues.
- Bjarki will finish class diagram and add structure to the project.
- We will make sure to add the markdown version of the rubrik to the repository.

## Decisions
- Design of ClapIT will be 3-tiered.

## For the Next Meeting (2023/09/22)
- Look at unittests.
- Split up tasks.
- Planning poker
- Look at gRPC.

Secretary: @bjarkithordarson
Supervisor: @hilmir21

## Meeting Date: 2023/09/19
Present: @bjarkithordarson, @SpicyBeard, @dadimarpatti, @LampCat, @hilmir21
Absent: None
Supervisor: @hilmir21
Secretary: @bjarkithordarson

## Meeting Progression
- Rubric was reviewed.
- Split user stories into tasks.
- Project structure reviewed.
- Played Planning Poker.
- Set up gRPC and made sure everyone was able to run the repository.
- Tasks were created under issues in GitLab.
- Decided to meet on Sunday afternoon to finish tasks.
- Scrum master updated project owner.

## Decisions
- Meeting on Sunday afternoon to finish tasks.

## For the Next Meeting (2023/09/24)
Supervisor: Scrum master @dadimarpatti
Secretary: @hilmir21
