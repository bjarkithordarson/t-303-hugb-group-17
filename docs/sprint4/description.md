# Sprint 4 Description
(Version 1)

## 1. Deadlines
-  23rd October for H1 and H3
-  24th October for H2, H4 and Akureyri
-  25th October for HMV

All deadlines are 23:59 UTC.

## 2. Activities at a Glance
1. **Plan** your sprint. Details are covered in Section 3.
	1. Randomly choose a Scrum Master
	2. Estimate and choose user stories for Sprint 3
	3. Include at least one user story related to the newly-introduced weather service.
	4. Agree on a definition of done for the sprint
	5. Break down user stories into tasks and divide among team members
2. **Implement** more functionality. Details are covered in Section 4.
	1. Implement the tasks/user stories for Sprint 4.
	2. Address at least one comment from the peer reviews.
	3. Provide automated unit tests with at least 50% statement coverage.
	4. Comment your code with docstrings.
	5. Leave instructions how to run your application.
3. **Maintain** the decision protocol. See Section 6.

## 3. Sprint Planning
You will **start Sprint 4 with a sprint planning session**. The tasks you need to do are identical to Sprint 2. Therefore, they are not repeated here. Consult the Sprint 2 description for details, if needed.

One addition to Sprint 4 is that we introduce a service providing weather data. You need to come up with and include at least one user story related to this new service.
In case you already use similar data, you may refactor your code to instead use the new service. 
Details on the service are described in Section 5.

## 4. Continued Implementation
During Sprint 4, you will **continue implementing** the application that you have proposed, based on your sprint planning. You proceed in the same way as in Sprint 2. Keep providing automated tests so that you have at least 50% statement coverage. Similarly, keep commenting your code and modify instructions on how to run the application, if necessary.

The only addition is that you need to address at least one comment from the peer reviews. Make it clear in your decision protocol which one that is. In case you believe none of the comments can or should be addressed, make clear why.

## 5. Weather Service

We provide a service providing weather data via a gRPC interface.
The source code is not provided, so you have to treat it as a black box.
The service will provide three remote procedure calls, one to obtain the current weather (temperature, humidity, cloud cover, wind speed, and wind direction) for a given latitude/longitude, one to obtain a forecast for a given latitude/longitude, and one to obtain historical weather for a given place and timespan.
Details on the technical usage of this service will be provided in a separate document soon, to not clutter this description.

## 6. Maintain the Decision Protocol
**Keep maintaining the decision protocol** that you have started during Sprint 1.
 
## 7. Submission and Assessment
Submission and assessment works as in Sprint 1.
Please carefully read the rubric that is published together with this assignment.