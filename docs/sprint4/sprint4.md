## Meeting Date: 2023/10/10
Present: Daði R, Daði M, Hilmir, Aron  
Absent: Bjarki 
Supervisor: Daði M  
Secretary: Hilmir  

## Meeting Progression
* Post-sprint reflections discussed
* Post code review discussed
* Accuracy of estimates: Were accurate since we were capable of completing our priority 1 tasks


## Decisions
* Scrum-master Dadi R
* Get a general overview of the code review and make decisions on tasks based on them see about assigning tasks or not etc
* review the weather service report part make tasks based on that
*Review what we had done on priority 2-3 tasks


## For the Next Meeting (2023/09/28)
Secretary: Hilmir 
Supervisor: Dadi M. 


## Meeting Date: 2023/10/12 (Thursday)

### Code Reviews:
- **Ticket Controller:**
  - Avoid instantiating the ticket model in the controller constructor; instantiate it in each method that needs it.
  - Rectify the issue where "Get ticket status time" was functioning incorrectly.

- **User Controller:**
  - Address the antipattern in the "get favorite bus stop" method, ensuring consistent return values.
  - Resolve the antipattern in user login, avoiding the return of two types of values.

- **Generic Model:**
  - Address the issue of single-letter variables.
  - Adjust the return types for generic getter & one-to-one getter.

- **Generic IO:**
  - Rectify the problem where the max tries parameter is not being passed to the recursive call in `__read_file`.

### Code Reviews for Tests:
- Tests are a work in progress. While type hinting is desirable, it's considered a lower priority as it doesn't significantly impact current outcomes.

### Code Review for gRPC:
- Points discussed during the meeting will be considered when working on this part of the system.

### Weather Service Sprint Review:
- Reviewed the weather service portion of the sprint and split it into tasks.

### Planning Poker:
1. Fix get_favorite_bus_stop return if no bus stop found: Time 15 min
2. If user login finds no user, raise exception instead of returning None: Time 15 min
3. Do not instantiate models inside controller constructors: Time 30 min
4. Create class-level docstrings: Time 15 min
5. Fix single letter variable names in General Model: Time 15 min
6. Fix GenericModel's generic_getter and one_to_one_getter return types: Time 30 min
7. Fix GenericIO __read_file recursive call, max_tries is never passed back into the method: Time 15 min
8. Create weather controller get_current_weather: Time 60 min
9. Create weather controller get_four_day_forecast: Time 60 min
10. Create weather controller get_historical_forecast: Time 60 min
11. Setup gRPC client for weather-service: Time 120 min

### Task Assignments:
- Task 11: Dadi R
- Task 8: Bjarki
- Task 10: Dadi M
- Code Review Tasks: Aron
- Task 9: Hilmir

## Next meeting:
Secretary: Bjarki
Supervisor: V. Aron
