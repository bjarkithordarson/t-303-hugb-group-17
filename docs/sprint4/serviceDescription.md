# Weather Service - Technical Description
(Version 1)

## 1. Overview

From Sprint 4 onwards, we provide a weather service that you have to use in your project.
Below, we describe the technical setup, the provided interface, and some rules regarding usage.

## 2. Technical Setup

The weather service is a Node.js/JavaScript application that is deployed on the Google Cloud.
It provides a gRPC interface, similar to what you have already seen and used in Sprints 2 and 3.
Internally, the service connects to Open Weather Map (<https://openweathermap.org>) and uses its API.
Just like any early application, this **service is under active development**. That means it might have bugs, be occassionaly unresponsive, or might change while you use it.

## 3. Provided Interface

The weather service exposes three RPCs via a gRPC interface. These are as follows:

1. get\_current\_weather (Location) returns (CurrentWeather)
2. get\_four\_day\_forecast (Location) returns (MultiForecast)
3. get\_historical\_forecast (HistoryPlace) returns (MultiForecast)

**get\_current\_weather** provides the current weather at a given location (geographical latitude and longitude). The current weather consists of temperature (in celsius), humidity and cloud cover (in percent), wind speed (in km/h) and wind direction (in degrees).

**get\_four\_day\_forecast** provides the hourly four-day forecast for a given location. The return value is an array with 96 elements, each of which contains the weather for a specified hour.

**get\_historical\_forecast** provides the historical weather in a given time period for a provided location string. The location string contains of city name and country code (e.g., "Reykjavik,IS" without spaces), while the time period is specified as start and end times as Unix time stamps (see, e.g., <https://www.unixtimestamp.com/>). The return value is an array with hourly weather descriptions. The RPC provides a maximum of 1-year old data, and for a maximum of a 24-hour period.

All three RPCs require additionally an API key. Each group will receive one such key in email, and will use that key only.

## 4. Usage and Rules

In terms of technical usage, we provide an example client that calls all three RPCs and provides a sample output. You can use that as a starting point to familiarise yourself with the service, and decide which RPCs to use in your code. In order to get the sample client to work, you need to enter your group's API key. The client code is located on <https://gitlab.com/grischal/hugb23-weather-service-client>.

Further detailed usage instructions and rules are as follows:

1. As stated in Section 2, there might be changes to the service. In particular, the service URL could change. We will update the client code and notify on Piazza in case changes occur.
2. As the service can change, and as it could contain bugs, make sure your code can handle errors. The sample client contains code that catches errors from the server.
3. The sample client contains a proto file that you should use as a detailed guidance on the parameters and return values you receive.
4. While we took basic precautions, there is a realistic chance that the service contains security vulnerabilities. We expect that you do not actively try to cause damage or annoyance. In cases were we suspect such misuse, we reserve the right to fail individuals or an entire group. We are logging usage extensively towards this end.
