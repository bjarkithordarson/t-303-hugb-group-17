# GTFS
GTFS defines a common format for public transportation schedules and associated geographic information. 
It is for example used by Google.

In this data package, all stops are listed with location along all routes and arrival and departure times at each bus stop for every day. 
The route lines are also specified for each route

## agency

The agency table provides information about the transit agency as such, including name, website and contact information.

Required fields:

    agency_name
    agency_url
    agency_timezone

## routes

The routes table identifies distinct routes. This is to be distinguished from distinct routings (or paths), several of which may belong to a single route.

Required fields:

    route_id (primary key)
    route_short_name
    route_long_name
    route_type

## trips

Required fields:

    trip_id (primary key)
    route_id (foreign key)
    service_id (foreign key)

Optional fields:

    block_id - The block ID indicates the schedule block to which a trip belongs.

## stop_times

Required fields:

    stop_id (primary key)
    trip_id (foreign key)
    arrival_time
    departure_time
    stop_sequence

Note that dwell time may be modelled by the difference between the arrival and departure times. However, many agencies do not seem to model dwell time for most stops.


## stops

The stops table defines the geographic locations of each and every actual stop or station in the transit system as well as, and optionally, some of the amenities associated with those stops.

Required fields:

    stop_id (primary key)
    stop_name
    stop_lon
    stop_lat

## calendar

The calendar table defines service patterns that operate recurrently such as, for example, every weekday. Service patterns that don't repeat such as for a one-time special event will be defined in the calendar_dates table.

Required fields:

    service_id (primary key)
    Monday
    Tuesday
    Wednesday
    Thursday
    Friday
    Saturday
    Sunday
    start_date
    end_date
	
	
## shapes
Rules for drawing lines on a map to represent a transit organization's routes.