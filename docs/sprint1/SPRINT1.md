# Meeting Notes

## Meeting 2023/8/31
**Present**: @dadimarpatti, @SpicyBeard, @LampCat, @hilmir21, @bjarkithordarson

**Supervisor**: @SpicyBeard 

**Secretary**: @bjarkithordarson

### Meeting progression
* Bjarki setup the GitLab repository and shared with other team members, teachers and TAs
* Everybody setup their GitLab accounts
* Daði M and Aron presented their manuals
* One meeting each week, thursdays?
* Everybody shared their phone numbers and class schedules

### Decisions
* Meet once a week on thursdays (including lab sessions)
* Each meeting has an assigned secretary
* Each meeting has a supervisor
* Stakeholders: Users (Passenger, Driver, System admin), Project Owner(TA), Strætó, Investors, SSH (Municipalities), Ministry of Transportation

### For next meeting (2023/8/5)
* Elicit requirements from TA
* Look at GenderMag and BrightSparks

# Requirements
* User signup
* User login
* Add/remove favorite bus stops
* View bus stop status (realtime, as in the bus stops)
* View busses on a map
* View bus stop schedules
* Plan a trip/auto trip finder
* Buy tickets
* Use tickets

# User Stories
**As a** commuter, **I want to** be able to be able to see the status of my most frequented bus stops at a glance **so that** I can know when the bus leaves as I run out.

**As a** commuter, **I want to** be able to see the positions of the busses on a map **so that** I can know if I missed my next bus.

**As a** commuter, **I want to** be able to check schedules of specific bus stops **so that** I can plan a trip better.

**As a** commuter, **I want to** be able to see whether the bus is late, on time or ahead of schedule **so that** I don't have to stand in the (sideways) rain for too long.

**As a** commuter, **I want to** be able to plan a trip, **so that** planning a trip is less stressful/time consuming.

**As a** commuter, **I want to** be able to buy and use tickets in the app **so that** I don't have to carry more things in my pocket.

## Meeting 2023/9/5
**Present**: @dadimarpatti, @SpicyBeard, @LampCat, @hilmir21, @bjarkithordarson, PO(Þór)

**Supervisor**: @SpicyBeard 

**Secretary**: @hilmir21

### Meeting progression
* PO gave feedback on progress so far
* PO answered questions about specifics regarding user stories, API, product overview, organization of documentation, Domain model, stakeholders. 

### Decisions
* When ready, Product overview will be linked in the Decicion Protocol
* Meet next thursday in the alloted slot for HUGB lecture to further work on the project
* @dadimarpatti will be supervisor and @LampCat will be secretary in the next meeting
* @SpicyBeard will add user stories to Issues in GitLab before next meeting
* Use a class diagram as a domain model

### For next meeting (2023/09/7)
* Create personas using GenderMag for the scenarios/user stories
* Work on scenarios for the project
* Create a domain model

## Meeting 2023/9/7
**Present**: @dadimarpatti, @SpicyBeard, @LampCat, @hilmir21, @bjarkithordarson

**Supervisor**: @LampCat 

**Secretary**: @dadimarpatti

### Meeting progression
* We've used the GenderMag tool to create personas
* Started working on the Project overview report for sprint 1
* Link to the **Project overview report** https://reykjavikuniversity-my.sharepoint.com/:w:/g/personal/bjarkit22_ru_is/Eda0rKHaSxJKkMzxN_cbMs8BcJwbxBnhq0WdEleb-s_8fQ?rtime=fjnDZ7Ov20g
* Added 2023/9/11: See deliverables below for a PDF of the report.
* @bjarkithordarson initiated the development of the domain model on the whiteboard, with valuable input and assistance from fellow team members.


### Decisions
* We've made the decision to create three personas initially, with the possibility of adding more if necessary 
* For the domain model, we opted to create an initial, basic class diagram
* We've assigned scenarios as homework for the upcoming meeting
* @hilmir21 Will be supervisor and @SpicyBeard will be secratary in the next meeting 


### For next meeting 2023/9/11
* Our next meeting is scheduled for Monday. We'll be in contact on Sunday to confirm our meeting details.
* Finish the domain model and add it to the report
* @dadimarpatti Will do scenarios
* @SpicyBeard Will finish the personas
* @bjarkithordarson Will go over the report
* @hilmir21 is going to finish the domain model
* @LampCat is going to add more to the User Stories

# Deliverables for Sprint 1
**Project Overview Report:** https://gitlab.com/bjarkithordarson/t-303-hugb-group-17/-/blob/a5fa50da9eee4929857487d9286d6717986511b4/docs/sprint1/Group_17_Project_Overview_Report.pdf

