[[_TOC_]]

# Grading
## Definition of done
### Pass
[ ] There has been an agreed-upon definition of
done (DoD) in the sprint. This DoD might not
have been reached, but the progress has
clearly been in that direction.
### Conditional pass
N/A
### Fail
There has not been any kind of DoD in the
sprint. The group proceeded without clear aim
what to deliver in the end.
## Sprint planning
### Pass
The sprint has been planned – user stories
have been suggested to the TA (and approved
by them). The implementation follows this
plan. Note that this does not mean that the
entire plan has been completed – only that
the team has not done entirely different
work.
### Conditional pass
N/A
### Fail
No suggested/approved sprint plan OR
completely different implementation focus
than committed to.
## Estimation
### Pass
At least the user stories that are included in
the sprint have an estimate in the form of
time or story points.
### Conditional pass
Individual estimates are missing, but at least
half of the planned stories have an estimate.
### Fail
Less than half of the planned user stories are
estimated.
## Task breakdown
### Pass
User stories have been broken down into
tasks by the group and assigned to group
members.
### Conditional pass
Team member responsibilities are clear, but
tasks might not exist for all user stories for
good reasons (e.g., small size of stories).
### Fail
No user story breakdown exists.
## Test coverage
### Pass
The system is covered by at least 50%
statement coverage (only the application
folder, not counting tests or external
libraries). A coverage report is in the
repository.
### Conditional pass
The system is covered by at least 40%
statement coverage (only the application
folder, not counting tests or external libraries)
OR the coverage report is not available.
### Fail
The system has less than 40% statement
coverage.
## Comments
### Pass
The system code is commented sufficiently
using docstrings to enable the TA to
understand the functionality.
### Conditional pass
The system code is commented, but there are unclear parts that lack comments OR docstrings have not been used throughout the code.
### Fail
There are no or only trivial comments (i.e., no actual explanation of how things are done).
## Readme
### Pass
README file exists in the project root and includes instructions how to install and run the application and the tests.
### Conditional pass
README file exists, but either in the wrong location OR lacks instructions on either installation, or execution.
### Fail
README does not exist, or does not contain any of the required instructions.
## Technical requirements
### Pass
Python, unittest, gRPC, and coverage are used to implement the system.
### Conditional pass
N/A
### Fail
The prescribed technology has been ignored and replaced with other libraries/frameworks.
## Decision protocol
### Pass
Decision protocol exists and contains high-level points that emable the TA to understand the group's main decisions.
### Conditional pass
Decision/standup protocol exists, but is lacking in clarity.
### Fail
No decision protocol or no/only trivial content (e.g., only decisions about when to meet - no project-specific content)
## Code Review (assessed individually)
### Pass
A code review has been added by each group member, as an issue in the GitLab target repository. It contains all points required in the sprint description.
### Conditional pass
A code review has been added by each group
member, as an issue in the GitLab target
repository. It misses one of the points required
in the sprint description.
### Fail
No code review has been performed OR the
review misses several required points.

# Continuous Progress
## Continuous Progress (+1 on final grade)
### Continuous progress shown
* In the mid-sprint meeting, the group can clearly show progress in the deliverables. That is, there exists commented code AND corresponding tests. Things can be missing or be "wrong" (e.g. not everything needs to be in running state, but it's obvious that work is already ongoing).
### No continuous progress shown
The group can’t show progress in the
deliverables. Questions might exist, but no
documented evidence (in the form of
deliverables) exist, yet. Having created the
required documents/files, but without
meaningful content is also considered to be
“no progress”.
## Code Review Access
### Continuous progress shown
All GitLab user names of the group have been
sent to the group that needs to provide access
AND reported access has been provided to all
members of the reviewing group (if that group
has sent their names).
### No continuous progress shown
User names have not been sent out OR access has not been provided.
