# Sprint 3 Description
(Version 1)

## 1. Deadlines
-  9th October for H1 and H3
-  10th October for H2, H4 and Akureyri
-  11th October for HMV

All deadlines are 23:59 UTC.

## 2. Activities at a Glance
1. **Plan** your sprint. Details are covered in Section 3.
	1. Randomly choose a Scrum Master
	2. Estimate and choose user stories for Sprint 3
	3. Agree on a definition of done for the sprint
	4. Break down user stories into tasks and divide among team members
2. **Implement** more functionality. Details are covered in Section 4.
	1. Implement the tasks/user stories for Sprint 3
	2. Provide automated unit tests with at least 50% statement coverage
	3. Comment your code with docstrings.
	4. Leave instructions how to run your application.
3. **Perform a code review** for another group. Details are covered in Section 5.
4. **Maintain** the decision protocol. See Section 6.

## 3. Sprint Planning
You will **start Sprint 3 with a sprint planning session**. The tasks you need to do are identical to Sprint 2. Therefore, they are not repeated here. Consult the Sprint 2 description for details, if needed.

The only addition to the planning done Sprint 3 is that you should try to take into account last sprint. That is, think about the following questions:

1. How accurate were your estimates? 
2. Did you plan too much or too little?
3. Did anything change in your understanding of the product?
4. Do you need to update the product backlog based on this?

## 4. Continued Implementation
During Sprint 3, you will **continue implementing** the application that you have proposed, based on your sprint planning. You proceed in the same way as in Sprint 2. Keep providing automated tests so that you have at least 50% statement coverage. Similarly, keep commenting your code and modify instructions on how to run the application, if necessary.

## 5. Perform a Code Review
In order to get better at reading other developers’ code, we ask you to **conduct a code review** in this sprint. Each group will review the code of another group that works on the same product, and each group member needs to do review work. 

Given that there is limited time, we ask each group member to **review a total of 60 minutes**. In contrast to "real-life" code reviews, we will not be reviewing new commits, features, or pull/merge requests, but simply the existing code. As a starting point for what to review, we suggest that you choose an implemented user story and try to understand how it works.

To review the code, a reviewing group X needs access to the repository of the group Y they should review. To organise this, group X’s ScrumMaster for Sprint 3 sends a message/mail to group Y’s ScrumMaster with the GitLab emails/usernames of all group members. Group Y’s ScrumMaster is then responsible that those users are added to the repository - reporter access is sufficient, guest access is not.

Once the users have been added, they can begin to review the code. It is up to each individual to choose which part of the code/user story to review. Once the review is done, the reviewer creates a new issue in the target repository in which they summarise the review. The review summary issue will be each individual’s evidence that they have performed the review. Points to include in the review are:

-   Which files or user stories have been reviewed

-   Overall impression

-   Good things about the code

-   Things to improve

-   An assessment of maintainability antipatterns

Regarding the last point, we expect you to familiarise yourself with the Python maintainability anti patterns located at <https://docs.quantifiedcode.com/python-anti-patterns/maintainability/index.html>. You will then have to comment to what extent these are found in the code you reviewed.

As overall pointers what to look for in a code review, consider the overview at <https://www.perforce.com/blog/qac/9-best-practices-for-code-review>. You are additionally encourage to use the code reading strategies we will discuss in the lecture on Design (on 2nd Oct).
Note that we expect you to be positive and constructive - remember that not everybody has as much experience or knowledge in programming as you might have. In particular, violations of the code of conduct (see <https://gitlab.com/grischal/hugb2023template/-/blob/master/code-of-conduct.md>) will lead to a fail.


The reviews will be done as follows. The arrow indicates which group reviews which other group.

-   **Travelator:** G2 -> G4 -> G5 -> G8 -> G10 -> G14 -> G15 -> G18 -> G22 -> G23 -> G24
-   **Mood Music:** G1 -> G6 -> G9 -> G11 -> G12 -> G13 -> G16 -> G19 -> G20 -> G25
-   **Clap-IT:** G3 -> G7 -> G17 -> G21

The last group reviews the first group. For example, G2 is reviewed by group G24, and performs a review for group G4.

## 6. Maintain the Decision Protocol
**Keep maintaining the decision protocol** that you have started during Sprint 1.

## 7. Technical Design Considerations
The following additions are made to allowed libraries/dependencies:

- You are allowed to use ``grpcio-reflections``.
- You are allowed to use libraries necessary to make HTTP(s) calls (such as ``requests``)

 
## 7. Submission and Assessment
Submission and assessment works as in Sprint 1. Do not forget the peer evaluation.
Please carefully read the rubric that is published together with this assignment.