## Meeting Date: 2023/09/26
Present: Daði R, Daði M, Bjarki, Hilmir, Aron  
Absent: None  
Supervisor: Daði M  
Secretary: Hilmir  

## Meeting Progression
* Post-sprint reflections discussed
  * Accuracy of estimates: Overly optimistic due to not accounting for system-learning time
  * Planned too much or too little: Just about right
  * New understandings: Unveiled hidden complexities
  * Update on product backlog: To revisit on next Thursday's meeting
* Decision to start work earlier in the next sprint
* Agreed on a 1-hour limit for the next planning poker session

## Decisions
* Decided to be more cautious with future estimates
* Agreed to begin work earlier in the next sprint
* Decided on a 1-hour limit for the next planning poker session

## For the Next Meeting (2023/09/28)
Secretary: Daði R  
Supervisor: Bjarki  

<<<<<<< HEAD

## Meeting Date: 2023/09/28
Present: Bjarki, Dadi R., Hilmir, Aron
Absent: Dadi M.
Supervisor: Bjarki
Secretary: Dadi R.
Sprint manager: Hilmir

## Meeting Progression
- Code Review:
  1. We do not have access yet.
  2. They do not have access yet. (Absence of response on their part)
- Deciding on User Stories:
  1. **Issue 7 - Bus Status for Commuters:**
     - Bus Stop Model
       - Read JSON from strætó
       - Define model
       - Populate model
       - Find by ID
     - BusStopController
       - get_bus_stop
       - get_bus_stop_notification
     - BusStopNotificationMDL
     - BSNCtrl
       - create_notification
       - delete_notification
  2. **Issue 8 - Buses on Map for Commuters:**
     - BusModel
       - Read JSON
       - Define Model
       - Populate Model
       - Find by ID
     - RouteModel
       - JSON
       - Busses
       - Define model
       - Populate, find_by_id
     - RouteController
       - get_busses
     - Grpc
       - get_route_busses
       - get_bs_notifications
       - create_notifications
       - delete_notifications
       - get_bustStop
     - Breaking this down into tasks:
       - A) GRPC connections
       - B) Create Bus Model
       - C) Create Route Model
       - D) Create Route Controller
       - E) Create BusStopModel
       - F) THEN Create BustStopController
       - G) Create BustStopNotification Model

## Planning Poker Outcomes:
- A) First outcome - 1hour, 1.5hours x 2, 2hours: Final 1.5 hours
- B) First outcome - 1.40hours, 150min, 3 hours, 2 hours - Second pass: 3 x 150min, 3 hours. Final 150 min
- C) First outcome - 150min 4x Final 150
- D) First outcome - 45.9min, 120min, 90min, 60min - second pass: 45min, 60min x3. Final 55min
- E) First outcome - 90min, 2.5hours x2, 2 hours - second pass: 149.7min, 120min, 130min, 140min. Final 130 min
- F) First outcome - 60 min x3, 50 min Final 50min
- G) First outcome - 90-120min, 119.8, 120min x 2 Final 120

## Task Priorities:
- A) GRPC connections - Priority 3
- B) Create Bus Model - Priority 1
- C) Create Route Model - Priority 1
- D) Create Route Controller - Priority 2
- E) Create BusStopModel - Priority 1
- F) Create BustStopController - Priority 2
- G) Create BustStopNotification Model - Priority 3

## Task Assignments:
- Task B-C: Aron
- Task E: Dadi R
- Task A: Hilmir
- Task G: Dadi M
- Task D-F: Bjarki

## Next Meeting :
Supervisor: Dadi R
Secretary: Aron
=======
## Meeting Date: 2023/10/03
Present: Daði R, Daði M, Bjarki, Hilmir, Aron  
Absent: None  
Supervisor: Daði R
Secretary: Aron 

## Meeting Progression
* Talked about not being able to get access for the code review for the other group.
* Checking on the rubric on what we have and what we don't have.
* Check if we have any questions for the TA.
* Talked about the code review and workload.
* Showed the TA the tasks we are going to take.
* Talked to the TA about the estimate of tasks.
* Asked the TA about how the code review works and how we hand it in.


## Decisions
* Try to meet up this 

>>>>>>> 087a6ebe70afbd2d33e4f7631f4198f295059f24
