## Meeting Date: 2023/10/24
Present: Daði R, Daði M, Hilmir,  Bjarki
Absent: Aron 
Supervisor: Hilmir  
Secretary: Dadi R 

## Meeting Progression
* Post-sprint reflections discussed
* Post code review discussed
* Accuracy of estimates



## Decisions
*Review code of conduct from week 1 - project overview file
*review the templates given from spring 5
*decide on scrum master

## For the Next Meeting (2023/10/26)
Secretary: Dadi R 
Supervisor: Bjarki


## Meeting Date: (2023/10/26)
Present: Aron, Bjarki, Daði M, Daði R, Hilmir
Absent:
Supervisor: Bjarki
Secretary: Daði 

## Meeting Progression
*Review user stories
*Tasks
*Estimation
*CI/CD

## Decisions
*Feature Freeze pre release
*Scrum master - Bjarki
*Tasks - Finalize ap 
    1. Use Ticket / Get ticket status API connection
        Estimate 90min
    2. Get favorite bust stop / get bus station status API connection
        Estimate 90min
    3. Get user by username / update user API connection
        Estimate 90min
    4. **Extra task ** low priority - Delete user/ user login API Connection
        Estimate 90min    
    5. CI/CD: Pipeline 
        Estimate 135min
    6. CI/CD: Coverage status
        Estimate 135min
Asignees 
    1.Hilmir
    2.Dadi R
    3.Dadi M
    5.Aron
    6.Bjarki
    4. Up for grabs if tasks are done quickly

## For the Next Meeting
*Verify DoD with PO
*Review tasks
