# Sprint 5 Description
(Version 1)

## 1. Deadlines
-  6th November for H1 and H3
-  7th November for H2, H4 and Akureyri
-  8th November for HMV

All deadlines are 23:59 UTC.

## 2. Activities at a Glance
1. **Plan** your sprint. Details are covered in Section 3.
	1. Randomly choose a Scrum Master
	2. Estimate and choose user stories for Sprint 3
	3. Agree on a definition of done for the sprint
	4. Break down user stories into tasks and divide among team members
2. **Implement** more functionality. Details are covered in Section 4.
	1. Implement the tasks/user stories for Sprint 5.
	2. Provide automated unit tests with at least 50% statement coverage.
	3. Comment your code with docstrings.
	4. Add continuous integration (CI) with pipeline and coverage badges.
	5. Leave instructions how to run your application.
3. **Maintain** the decision protocol. See Section 6.

## 3. Sprint Planning
You will **start Sprint 5 with a sprint planning session**. The tasks you need to do are identical to Sprint 2. Therefore, they are not repeated here. Consult the Sprint 2 description for details, if needed.

## 4. Continued Implementation
During Sprint 5, you will **continue implementing** the application that you have proposed, based on your sprint planning. You proceed in the same way as in Sprint 2. Keep providing automated tests so that you have at least 50% statement coverage. Similarly, keep commenting your code and modify instructions on how to run the application, if necessary.

The only addition is that you need to **add CI to your GitLab project**. The CI pipeline needs to run your unit tests and coverage analysis for all branches. Furthermore, for the GitLab repository, you need to add badges that report the pipeline status and the overall coverage percentage for the main/master branch. Information on how to do this will be coverage in the lecture on Module 11. Furthermore, you can consult the following documentation:

- General CI/CD documentation for GitLab: <https://docs.gitlab.com/ee/ci/>
- GitLab Badges documentation: <https://docs.gitlab.com/ee/user/project/badges.html>
- GitLab coverage report documentation: <https://docs.gitlab.com/ee/ci/testing/code_coverage.html#view-code-coverage-results-in-the-mr>

## 5. Maintain the Decision Protocol
**Keep maintaining the decision protocol** that you have started during Sprint 1.
 
## 6. Submission and Assessment
Submission and assessment works as in Sprint 1.
Please carefully read the rubric that is published together with this assignment.