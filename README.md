# Hugb2023Template

This is a template project that contains the structure for the project work in T-303-HUGB Software Engineering, Fall 2023.

Please make sure to read the [Code of Conduct](https://gitlab.com/grischal/hugb2023template/-/blob/main/code-of-conduct.md).

## First setup
To setup the virtual environment, run: 
```
python -m venv .venv
```
Activate the virtual environment. You must do this every time you start developing.

Windows:
```
cd /path/to/project
.venv/Scripts/activate.ps1
```

Mac:
```
cd /path/to/project
source .venv/bin/activate
```

Install the dependencies:

Windows:
```
cd /path/to/project
pip install -r requirements.txt
```

Mac:
```
cd /path/to/project
pip3 install -r requirements.txt
```

## CLI
```
python -m src.cli METHOD_NAME [ARGS...]
```
For general help, run:
```
python -m src.cli --help
```
For help with specific command, run:
```
python -m src.cli METHOD_NAME --help
```
Example use:
```
python -m src.cli get_four_day_forecast 64.21 -21.19
```

## Compile the gRPC files
```
cd /path/to/project
python -m grpc_tools.protoc --python_out=src/grpc_api/ --proto_path=src/grpc_api/protos --grpc_python_out=src/grpc_api ClapIT.proto
```

## Run the server
```
cd /path/to/project
python -m src.grpc_api.grpc_server
```

## Setup Postman
1. Open Postman
2. Click Workspaces and then Create Workspace.
3. Select a blank workspace, give it a name and click Create.
4. Next to the workspace name, click New and then gRPC.
5. Enter 127.0.0.1:8000 as the URL.
6. Postman should now automatically see the available methods on the server using reflection.

## Documentation
### Data layer
Please read datalayer.md for information on how to use the data layer.

## Tests
### Unit tests
```
python -m unittest discover
```

### Coverage report
```
coverage run -m unittest discover
python -m coverage html
```