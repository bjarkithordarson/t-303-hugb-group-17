from ..models.GenericModel import GenericModel

class UserModel(GenericModel):
    """The User model class.
    
    This class is responsible for creating and maintaining users."""
    @property
    def id(self):
        """Return the id of the user.

        Returns:
        --------
        str
          The id of the user.
        """
        return UserModel.generic_getter(self, "id")
    
    @id.setter
    def id(self, value):
        """Set the id of the user.
        
        Parameters:
        -----------
        value: str
          The id of the user.
        """
        return UserModel.generic_setter(self, "id", value)
    
    @property
    def username(self):
        """Return the username of the user.

        Returns:
        --------
        str
          The username of the user.
        """
        return UserModel.generic_getter(self, "username")
    
    @username.setter
    def username(self, value):
        """Set the username of the user.
        
        Parameters:
        -----------
        value: str
          The username of the user.
        """
        return UserModel.generic_setter(self, "username", value)
    
    @property
    def password(self):
        """Return the password of the user.

        Returns:
        --------
        str
          The password of the user.
        """
        return UserModel.generic_getter(self, "password")
    
    @password.setter
    def password(self, value):
        """Set the password of the user.
        
        Parameters:
        -----------
        value: str
          The password of the user.
        """
        return UserModel.generic_setter(self, "password", value)
    
    @property
    def email(self):
        """Return the email of the user.

        Returns:
        --------
        str
          The email of the user.
        """
        return UserModel.generic_getter(self, "email")
    
    @email.setter
    def email(self, value):
        """Set the email of the user.
        
        Parameters:
        -----------
        value: str
          The email of the user.
        """
        return UserModel.generic_setter(self, "email", value)
    
    @property
    def tickets(self):
        """Return the tickets the user has.

        Returns:
        --------
        list
          The ids of tickets the user has.
        """
        return UserModel.many_to_many_getter(self, "tickets")
    
    @tickets.setter
    def tickets(self, value):
        """Set the tickets of the user.
        
        Parameters:
        -----------
        value: list
          The tickets the user has.
        """
        return UserModel.many_to_many_setter(self, value, "tickets")
    
    @property
    def favorite_bus_stops(self):
        """Return a list of favorite bus stops the user has.

        Returns:
        --------
        list
          The ids of bus stops the user has as favorites.
        """
        return UserModel.many_to_many_getter(self, "favorite_bus_stops")
    
    @favorite_bus_stops.setter
    def favorite_bus_stops(self, value):
        """Set the favorite bus stops the user has.
        
        Parameters:
        -----------
        value: list
          The ids of the bus stop the user has as favorites.
        """
        return UserModel.many_to_many_setter(self, value, "favorite_bus_stops")
