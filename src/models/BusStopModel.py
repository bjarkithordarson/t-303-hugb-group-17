from models.GenericModel import GenericModel

class BusStopModel(GenericModel):
    @property
    def id(self):
        """Return the id of the bus stop.
        
        Returns:
        --------
        str
          The id of the bus stop.
        """
        return BusStopModel.generic_getter(self, "id")
    
    @id.setter
    def id(self, value):
        """Set the id of the bus stop.
        
        Parameters:
        -----------
        value: str
          The id of the bus stop.
        """
        return BusStopModel.generic_setter(self, "id", value)
    
    @property
    def name(self):
        """Return the name of the bus stop.
        
        Returns:
        --------
        str
          The name of the bus stop.
        """
        return BusStopModel.generic_getter(self, "name")
    
    @name.setter
    def name(self, value):
        """Set the name of the bus stop.
        
        Parameters:
        -----------
        value: str
          The name of the bus stop.
        """
        return BusStopModel.generic_setter(self, "name", value)
    
    @property
    def lat(self):
        """Return the lat of the bus stop.
        
        Returns:
        --------
        float
          The lat of the bus stop.
        """
        return BusStopModel.generic_getter(self, "lat")
    
    @lat.setter
    def lat(self, value):
        """Set the lat of the bus stop.
        
        Parameters:
        -----------
        value: float
          The lat of the bus stop.
        """
        return BusStopModel.generic_setter(self, "lat", value)
    
    @property
    def lon(self):
        """Return the lon of the bus stop.
        
        Returns:
        --------
        float
          The lon of the bus stop.
        """
        return BusStopModel.generic_getter(self, "lon")
    
    @lon.setter
    def lon(self, value):
        """Set the lon of the bus stop.
        
        Parameters:
        -----------
        value: float
          The lon of the bus stop.
        """
        return BusStopModel.generic_setter(self, "lon", value)
    
    @property
    def buses(self):
        """Return the buses of the bus stop.
        
        Returns:
        --------
        list
          The buses of the bus stop.
        """
        return BusStopModel.one_to_many_getter(self, "buses")
    
    @buses.setter
    def buses(self, value):
        """Set the buses of the bus stop.
        
        Parameters:
        -----------
        value: list
          The buses of the bus stop.
        """
        return BusStopModel.one_to_many_setter(self, value, "buses")
    
    @property
    def location_type(self):
        """Return the location_type of the bus stop.
        
        Returns:
        --------
        str
          The location_type of the bus stop.
        """
        return BusStopModel.generic_getter(self, "location_type")
    
    @location_type.setter
    def location_type(self, value):
        """Set the location_type of the bus stop.
        
        Parameters:
        -----------
        value: str
          The location_type of the bus stop.
        """
        return BusStopModel.generic_setter(self, "location_type", value)
    
    """populate model with data from database"""
    def populate(self, data):
        """Populate the model with data from the database.
        
        Parameters:
        -----------
        data: dict
          The data to populate the model with.
        """
        BusStopModel.generic_populate(self, data)
        BusStopModel.one_to_many_populate(self, data, "buses")
