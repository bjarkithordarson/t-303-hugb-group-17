from datetime import date
import json
from urllib.request import urlopen

class RouteModel:
    #Not sure what link to use here
    #https://opendata.straeto.is/data/web-data/bid/{route_id}
    #https://opendata.straeto.is/data/web-data/timatoflur/json/{route_id}
    def __init__(self, route_id):
        self.url = "https://opendata.straeto.is/data/web-data/timatoflur/json/" + str(route_id)
        res = urlopen(self.url)

        self.data = json.load(res)['leidir']
        for route in self.data:
            self.sv = route['sv']
            self.route = route['leid']
            self.route_id = route['route_id']
            self.hs = route['dirs'][0]['hs'] #The start and end of the bus route
            self.daygroups = route['dirs'][0]['daygroups']

    def get_day_group(self):
        current_day = date.today()
        day = current_day.weekday()
        if day == 5: #Saturday
            return self.daygroups[1]
        elif day == 6: #Sunday
            return self.daygroups[2]
        else:#Monday - Friday
            return self.daygroups[0]
    
    def get_bus_stops(self):#Return later the bustopModel with all the usfull info
        stops_to_return = []
        for stop in self.get_day_group()['stops']:
            stops_to_return.append(stop['stop_id'])
        return stops_to_return
    def __str__(self):
        return self.route + "(" + self.hs + ")"