from src.data.GenericIO import GenericIO

class GenericModel(object):
  """A generic model that can be used to create other models."""
  def __init__(self, data={}):
    """Initialize the model.

    Parameters:
    ----------
    data: dict
      A dictionary with the data that is being set.
    """
    self.data = {}
    for key in data.keys():
      setattr(self, key, data[key])

  def save(self):
    """Saves the model to the database.
    
    If the model is not in the database, it will be inserted.
    If the model is in the database, it will be updated.
    
    Raises:
    -------
    Exception
      If the model is not in the database.
    """
    io = GenericIO(self)
    results = io.find_by_key("id", self.id)
    if len(results) == 0:
      io.insert(self)
    else:
      io.update(self)

  def delete(self):
    """Deletes the model from the database.
    
    Raises:
    -------
    Exception
      If the model is not in the database.
    """
    io = GenericIO(self)
    io.delete(self.id)

  @classmethod
  def find_all(cls):
    """Returns all rows from the database.
    
    Returns:
    --------
    list
      A list of rows from the database.
    """
    io = GenericIO(cls())
    return io.find_all()

  @classmethod
  def find_by_id(cls, id:int):
    """Returns a row from the database with the given ID.
    
    Parameters:
    ----------
    id: int
      The ID of the row.
    """
    io = GenericIO(cls())
    results = io.find_by_key("id", id)
    if len(results) == 1:
      return results[0]
    else:
      raise Exception(cls.__name__ + " with id " + str(id) + " not found")

  @classmethod
  def find_by_key(cls, key:str, value:any):
    """Returns all rows that match the given key and value.
    
    Parameters:
    ----------
    key: str
      The key that is being matched.
    value: any
      The value that is being matched.
    """
    io = GenericIO(cls())
    return io.find_by_key(key, value)

  @classmethod
  def many_to_many_setter(cls, entity, value, attribute):
    """Defines the setter for a many-to-many relationship.
    
    Parameters:
    ----------
    entity: GenericModel
      The model that is being set.
    value: list
      A list of models.
    attribute: str
      The name of the attribute that is being set.
      
    Returns:
    --------
    list
      A list of references to the models.
    """
    if issubclass(type(value), GenericModel):
        entity.data[attribute] = value.to_ref()
    else:
        entity.data[attribute] = [item for item in value]

  @classmethod
  def many_to_many_getter(cls, entity, attribute):
    """Defines the getter for a many-to-many relationship.
    
    Parameters:
    ----------
    entity: GenericModel
      The model that is being set.
    attribute: str
      The name of the attribute that is being set.
      
    Returns:
    --------
    list
      A list of models."""
    return [cls.from_ref(item) for item in entity.data[attribute]]

  @classmethod
  def one_to_one_setter(cls, entity, value, attribute):
    """Defines the setter for a one-to-one relationship.
    
    Parameters:
    ----------
    entity: GenericModel
      The model that is being set.
    value: GenericModel
      A list of models.
    attribute: str
      The name of the attribute that is being set."""
    if issubclass(type(value), GenericModel):
        entity.data[attribute] = value.to_ref()
    else:
        entity.data[attribute] = value

  @classmethod
  def one_to_one_getter(cls, entity, attribute):
    """Defines the getter for a one-to-one relationship.

    Parameters:
    ----------
    entity: GenericModel
      The model that is being set.
    attribute: str
      The name of the attribute that is being set.

    Returns:
    --------
    GenericModel
      The model that is referenced by the attribute.
    """
    return cls.from_ref(entity.data[attribute]) if attribute in entity.data else None

  @classmethod
  def generic_setter(cls, entity, attribute, value):
    """Defines the setter for a generic attribute.
    
    Parameters:
    ----------
    entity: GenericModel
      The model that is being set.
    attribute: str
      The name of the attribute that is being set.
    value: any
      The value that is being set.
    """
    entity.data[attribute] = value

  @classmethod
  def generic_getter(cls, entity, attribute):
    """Defines the getter for a generic attribute.
    
    Parameters:
    ----------
    entity: GenericModel
      The model that is being set.
    attribute: str
      The name of the attribute that is being set.
      
    Returns:
    --------
    any
      The value of the attribute."""
    return entity.data[attribute] if attribute in entity.data else None

  def to_ref(self):
    """Returns a reference to the model.
    
    Returns:
    --------
    dict
      A dictionary with the type and id of the model."""
    return {
      "type": type(self).__name__,
      "id": self.id
    }
  
  @classmethod
  def from_ref(cls, ref):
    """Returns a model from a reference.
    
    Parameters:
    ----------
    ref: dict
      A dictionary with the type and id of the model.
      
    Returns:
    --------
    GenericModel
      The model that is referenced by the ref."""
    return cls.find_by_id(ref["id"])

  def __str__(self):
    return type(self).__name__ + ": " + str(self.data)
  

