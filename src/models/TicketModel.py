from src.models.GenericModel import GenericModel

class TicketModel(GenericModel):
    """The ticket model class.
    
    This class is responsible for creating and using tickets."""

    @property
    def id(self):
        """Return the id of the ticket.

        Returns:
        --------
        str
          The id of the ticket.
        """
        return TicketModel.generic_getter(self, "id")
    
    @id.setter
    def id(self, value):
        """Set the id of the ticket.
        
        Parameters:
        -----------
        value: str
          The id of the ticket.
        """
        return TicketModel.generic_setter(self, "id", value)

    @property
    def time_activated(self):
        """Return the time_activated of the ticket.
        
        Returns:
        --------
        datetime
          The time_activated of the ticket.
        """
        return TicketModel.generic_getter(self, "time_activated")
    
    @time_activated.setter
    def time_activated(self, value):
        """Set the time_activated of the ticket.
        
        Parameters:
        -----------
        value: datetime
          The time_activated of the ticket.
        """
        return TicketModel.generic_setter(self, "time_activated", value)
    
    @property
    def owner(self):
        """Return the owner of the ticket.
        
        Returns:
        --------
        UserModel
          The owner of the ticket.
        """
        return TicketModel.one_to_one_getter()
    
    @owner.setter
    def owner(self, value):
        return TicketModel.one_to_one_setter(self, value, "owner")
    
    @property 
    def ticket_price(self):
        """Return the ticket_price of the ticket.
        
        Returns:
        --------
        int
          The ticket_price of the ticket."""
        return TicketModel.generic_getter(self,"ticket_price")
    
    @ticket_price.setter
    def ticket_price(self, value):
        """Set the ticket_price of the ticket.
        
        Parameters:
        -----------
        value: int
          The ticket_price of the ticket.
        """
        return TicketModel.generic_setter(self,"ticket_price", value)
    


