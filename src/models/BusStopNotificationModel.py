from ..models.GenericModel import GenericModel

class BusStopNotificationModel(GenericModel):
    @property
    def id(self):
        return BusStopNotificationModel.generic_getter(self, "id")

    @id.setter
    def id(self, value):
        return BusStopNotificationModel.generic_setter(self, "id", value)
    
    @property
    def user_id(self):
        return BusStopNotificationModel.generic_getter(self, "user_id")

    @user_id.setter
    def user_id(self, value):
        return BusStopNotificationModel.generic_setter(self, "user_id", value)
    
    @property
    def bus_stop_id(self):
        return BusStopNotificationModel.generic_getter(self, "bus_stop_id")

    @bus_stop_id.setter
    def bus_stop_id(self, value):
        return BusStopNotificationModel.generic_setter(self, "bus_stop_id", value)
    
    @property
    def time(self):
        return BusStopNotificationModel.generic_getter(self, "time")

    @time.setter
    def time(self, value):
        return BusStopNotificationModel.generic_setter(self, "time", value)

    @property
    def status(self):
        return BusStopNotificationModel.generic_getter(self, "status")

    @status.setter
    def status(self, value):
        return BusStopNotificationModel.generic_setter(self, "status", value)
