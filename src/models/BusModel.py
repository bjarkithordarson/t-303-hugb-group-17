import models.BusStopModel as BusStopModel

class BusModel:
    #Myabe take the bus route here and just load that data everytime we create it 
    '''Takes in raw data from the json of the bus'''
    def __init__(self, data):
        self.head = data['head']
        self.feril = data['ferill']
        self.lat = data['lat']
        self.lon = data['lon']
        self.komur = data['komur']

    def get_closest_stop(self):
        '''Returns the closes stop information such as time, name and stopp id'''
        return self.komur[0]

    def get_all_bus_stops(self):
        '''Gets all the bus stops ids that this bus stops at'''
        bus_stop_ids =[]
        for stop in self.komur:
            bus_stop_ids.append(stop['stopp'])
        return bus_stop_ids

    def __str__(self):
        return self.feril + " " + str(self.lat) + " " + str(self.lon)