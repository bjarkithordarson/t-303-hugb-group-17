import sys
from inspect import getfullargspec, getdoc
from src.API.ClapITAPI import ClapITAPI

def get_public_methods(cls):
    return [method for method in dir(cls) if not method.startswith("_")]

def get_param_string(cls, method):
    argspec = getfullargspec(getattr(cls, method))
    params = []

    for param in argspec.args:
        p = param
        if param in argspec.annotations:
            p += ": " + argspec.annotations[param].__name__
        params.append(p)
    return "(" + ", ".join(params) + ")"

def get_method_list(cls):
    methods = get_public_methods(cls)
    method_list = []
    for method in methods:
        method_list.append(method + get_param_string(cls, method))
    return method_list

def get_docstring(cls, method):
    return getdoc(getattr(cls, method))

def get_help():
    help = "Usage: python -m src.cli METHOD_NAME [ARGS...]\n\n"
    help += "METHOD_NAME can be one of the following:\n"
    for method in get_method_list(ClapITAPI):
        help += "  " + method + "\n"
    help += "\n"
    help += "Use the --help flag to get help for a specific method:\n"
    help += "  python -m src.cli METHOD_NAME --help\n"
    return help

def main():
    # Check if no arguments
    if len(sys.argv) == 1:
        return get_help()

    # Check if first argument is --help
    if len(sys.argv) == 2 and sys.argv[1] == "--help":
        return get_help()

    # Get the first argument as the method name
    method_name = sys.argv[1]
    
    # Check if method exists in ClapITAPI
    if not hasattr(ClapITAPI, method_name):
        raise Exception("Method '" + method_name + "' does not exist in ClapITAPI")
    
    # Check if last argument is --help
    if len(sys.argv) >= 3 and sys.argv[-1] == "--help":
        help = getdoc(getattr(ClapITAPI, method_name))
        
        if help != None:
            return help
        else:
            raise Exception("No docstring found for method '" + method_name + "'")
    
    # Get the arguments for the method
    method_args = getfullargspec(getattr(ClapITAPI, method_name))
    #print(method_args)

    # Get the arguments from the command line
    args = sys.argv[2:]

    # Typecast the arguments
    type_args = []

    for i in range(len(args)):
        arg = args[i]
        arg_type = method_args.annotations[method_args.args[i]]
        type_args.append(arg_type(arg))

    # Call the method with the arguments
    return getattr(ClapITAPI, method_name)(*type_args)

if __name__ == "__main__":
    try:
        out = main()
        if out:
            print(main())
    except Exception as err:
        print(err)