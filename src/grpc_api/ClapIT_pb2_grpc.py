# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import ClapIT_pb2 as ClapIT__pb2


class ClapITStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.CreateUser = channel.unary_unary(
                '/clapit.ClapIT/CreateUser',
                request_serializer=ClapIT__pb2.ClapITUser.SerializeToString,
                response_deserializer=ClapIT__pb2.ClapITResponse.FromString,
                )
        self.CreateTicket = channel.unary_unary(
                '/clapit.ClapIT/CreateTicket',
                request_serializer=ClapIT__pb2.ClapITTicket.SerializeToString,
                response_deserializer=ClapIT__pb2.ClapITResponse.FromString,
                )


class ClapITServicer(object):
    """Missing associated documentation comment in .proto file."""

    def CreateUser(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def CreateTicket(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_ClapITServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'CreateUser': grpc.unary_unary_rpc_method_handler(
                    servicer.CreateUser,
                    request_deserializer=ClapIT__pb2.ClapITUser.FromString,
                    response_serializer=ClapIT__pb2.ClapITResponse.SerializeToString,
            ),
            'CreateTicket': grpc.unary_unary_rpc_method_handler(
                    servicer.CreateTicket,
                    request_deserializer=ClapIT__pb2.ClapITTicket.FromString,
                    response_serializer=ClapIT__pb2.ClapITResponse.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'clapit.ClapIT', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class ClapIT(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def CreateUser(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/clapit.ClapIT/CreateUser',
            ClapIT__pb2.ClapITUser.SerializeToString,
            ClapIT__pb2.ClapITResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def CreateTicket(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/clapit.ClapIT/CreateTicket',
            ClapIT__pb2.ClapITTicket.SerializeToString,
            ClapIT__pb2.ClapITResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
