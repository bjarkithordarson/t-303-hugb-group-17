from __future__ import absolute_import
from concurrent import futures
from src.API.ClapITAPI import ClapITAPI

import grpc
from . import ClapIT_pb2_grpc
from . import ClapIT_pb2

import grpc_reflection.v1alpha.reflection as grpc_reflection



API = ClapITAPI()

class ClapITServicer(ClapIT_pb2_grpc.ClapITServicer):    
    def CreateUser(self, request, context):
        print("Received new user from client: " + request.username)
        API.create_user(request.username, request.password, request.email)
        return ClapIT_pb2.ClapITResponse(message='User created successfully!')
    
    def CreateTicket(self, request, context):
        print("Received new ticket from client!")
        API.create_ticket(request.owner_id, request.ticket_price)
        return ClapIT_pb2.ClapITResponse(message='Ticket created successfully!')
    

    
def server():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    ClapIT_pb2_grpc.add_ClapITServicer_to_server(ClapITServicer(), server)

    grpc_reflection.enable_server_reflection(['clapit.ClapIT'], server)

    server.add_insecure_port('127.0.0.1:8000')
    print("Starting ClapIT server. Listening on port 8000.")

    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    server()