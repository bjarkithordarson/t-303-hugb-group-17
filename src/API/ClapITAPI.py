from src.controllers.WeatherController import WeatherController
from src.controllers.TicketController import TicketController
from src.models.TicketModel import TicketModel
from src.models.UserModel import UserModel

class ClapITAPI:
    def create_user(username:str, password:str, email:str):
        """
        Creates a new user and returns the user object.
        
        Parameters:
        -----------
        username (str): The username of the user.
        password (str): The password of the user.
        email (str): The email of the user.
        
        Returns:
        --------
        UserModel: The user object.
        
        Raises:
        -------
        ValueError: If the username, password or email is not a string.
        """

        user = UserModel()
        user.username = username
        user.password = password
        user.email = email
        user.save()
        return user
    
    def user_login(username:str, password:str):
        """ 
        Logs in the user and returns the user object.
        
        Parameters:
        -----------
        username (str): The username of the user.
        password (str): The password of the user.
        
        Returns:
        --------
        UserModel: The user object."""

        user = UserModel.find_by_username(username)
        if user.password == password:
            return user
        else:
            return None
        
    def delete_user(user:UserModel):
        """
        Deletes the user object from the database.
        
        Parameters:
        -----------
        user (UserModel): The user object.
        
        Raises:
        -------
        Exception: If the user is not in the database.
        """
        user.delete()
    
    def create_ticket(owner_id:int, price:int):
        """
        Creates a new ticket and returns the ticket object.

        Parameters:
        -----------
        owner_id (int): The id of the user that owns the ticket.
        price (int): The price of the ticket.

        Returns:
        --------
        TicketModel: The ticket object.
        """
        ticket = TicketController.create_ticket(owner_id)
        ticket.ticket_price = price

        return ticket


    def use_ticket(ticket_id):
        try:
            ticket = TicketController.get_ticket(ticket_id)
        except LookupError:
            print("Ticket not found!")

        return TicketController.use_ticket(ticket)


    def get_ticket_status(ticket_id):
        try:
            ticket = TicketController.get_ticket(ticket_id)
        except LookupError:
            print("Ticket not found!")

        return TicketController.get_ticket_status(ticket)

    def user_buy_ticket(user:UserModel,price:int):
        """
        User buys a ticket.
        
        Parameters:
        -----------
        user (UserModel): The user that buys the ticket.
        price (int): The price of the ticket.
        
        Returns:
        --------
        TicketModel: The ticket object.
        """
        ticket = ClapITAPI.create_ticket(user.id, price)
        user.tickets.append(ticket)
        user.save()
        return ticket


    def get_favorite_bus_stops(user_id:int):
        """
        Returns the user's favorite bus stops.

        Parameters:
        ---------- 
        user_id (int): The id of the user.

        Returns:
        --------
        list: A list of the user's favorite bus stops.
        """
        user_id = UserModel.find_by_id(user_id)
        return user_id.favourite_bus_stops


    def get_bus_stop_status():
        print("Bus stop status!")
        pass

    def get_current_weather(lat:float=64.21, lon:float=-21.19):
        """
        Returns the current weather for a given latitude and longitude.
        
        Parameters:
        -----------
        lat (float): The latitude of the location to get the weather for. Defaults to 64.21.
        lon (float): The longitude of the location to get the weather for. Defaults to -21.19.

        Returns:
        --------
        dict: A dictionary containing weather data for the specified location.

        Raises:
        -------
        ValueError: If the latitude or longitude is not a float.
        """

        return WeatherController.get_current_weather(lat, lon)
    
    def get_four_day_forecast(lat:float=64.21, lon:float=-21.19):
        """
        Returns a four-day weather forecast for a given latitude and longitude.
        
        Parameters:
        -----------
        lat (float): The latitude of the location to get the forecast for. Defaults to 64.21.
        lon (float): The longitude of the location to get the forecast for. Defaults to -21.19.
        
        Returns:
        --------
        list: A list of dictionaries containing weather forecast data for the next four days.
        
        Raises:
        -------
        ValueError: If the latitude or longitude is not a float.
        """
        return WeatherController.get_four_day_forecast(lat, lon)
    
    def get_historical_forecast(city_name:str, country_code:str, start_time:int=None, end_time:int=None):
        """
        Returns a historical weather forecast for a given city and time range.
        
        Parameters:
        -----------
        city_name (str): The name of the city to get the forecast for.
        country_code (str): The country code of the city to get the forecast for, in ISO 3166-1 alpha-2 format.
        start_time (int): The start time of the time range to get the forecast for, in Unix timestamp format.
        end_time (int): The end time of the time range to get the forecast for, in Unix timestamp format.
        
        Returns:
        --------
        list: A list of dictionaries containing weather forecast data for the specified city and time range.
        
        Raises:
        -------
        ValueError: If the start time or end time is not an integer.
        ValueError: If the end time is before the start time.
        """
        return WeatherController.get_historical_forecast(city_name, country_code, start_time, end_time)