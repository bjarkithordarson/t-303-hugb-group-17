import datetime
import uuid
from models.TicketModel import TicketModel
from controllers.TicketController import TicketController

if __name__ == "__main__":
    ticket = TicketModel()
    ticket.serial_number = str(uuid.uuid4())
    ticket.time_activated = str(datetime.datetime.now())
    ticket.save()

    ticket2 = TicketModel.find_by_key("serial_number", "ba2ce806-e6b4-4328-a76e-3872914c8194")

    for t in ticket2:
        t.delete()