from datetime import datetime
import random
import unittest

from API.ClapITAPI import ClapITAPI

class TestClapITAPI (unittest.TestCase):
    def setUp(self):
        """Sets up the test case by creating a new ClapITAPI object."""
        self.api = ClapITAPI()
        self.user = self.api.create_user("TicketTest", "password", "email@x.com")
        self.ticket = self.api.create_ticket(self.user.id, 999)

    def tearDown(self):
        """Tears down the test case by deleting the ClapITAPI object."""
        self.user.delete()
        self.ticket.delete()
        self.api = None

    def test_create_user(self):
        pass

    def test_create_ticket(self):
        """Tests the create_ticket() method of the Ticket class
        
        Creates a new user, creates a new ticket, and then checks that the
        ticket's owner is the user, the ticket_price is the same as the one
        passed in, and the time_activated is None.
        """
        self.setUp()

        # Generate a random ticket price
        ticket_price = random.randint(0, 1000)
        # Generate a random username
        username = "testUser" + str(random.randint(0, 1000))
        # Generate a random password
        password = "testPassword" + str(random.randint(0, 1000))
        # Generate a random email
        email = "testuser" + str(random.randint(0, 1000)) + "@dummy.invalid"
        # Create a test user
        user = ClapITAPI.create_user(username, password, email)
        # Create a test ticket
        ticket = ClapITAPI.create_ticket(user.id, ticket_price)

        #assert ticket.owner == user.id
        assert ticket.ticket_price == ticket_price
        assert ticket.time_activated is None

        user.delete()
        ticket.delete()

        self.tearDown()

    def test_delete_user(self):
        # Generate a random username
        username = "testUser" + str(random.randint(0, 1000))
        # Generate a random password
        password = "testPassword" + str(random.randint(0, 1000))
        # Generate a random email
        email = "testuser" + str(random.randint(0, 1000)) + "@dummy.invalid"
        # Create a test user
        user = ClapITAPI.create_user(username, password, email)
        # Assert that the user is in the database
        assert ClapITAPI.user_login(username, password)
        # Delete the test user
        ClapITAPI.delete_user(user)
        # Assert that the user is not in the database
        assert ClapITAPI.user_login(username, password) is None

    def test_user_login(self):
        # Generate a random username
        username = "testUser" + str(random.randint(0, 1000))
        # Generate a random password
        password = "testPassword" + str(random.randint(0, 1000))
        # Generate a random email
        email = "testuser" + str(random.randint(0, 1000)) + "@dummy.invalid"
        # Create a test user
        user = ClapITAPI.create_user(username, password, email)
        # Assert that the user is in the database
        assert ClapITAPI.user_login(username, password)
        ClapITAPI.delete_user(user)
        assert ClapITAPI.user_login(username, password) is None

        self.tearDown()

    def test_use_ticket(self):
        """Tests the use_ticket() method of the Ticket class
        
        Creates a new user, creates a new ticket, uses the ticket, and then
        checks that the ticket's time_activated is not None and is greater than
        the current time.
        """
        self.setUp()
        self.api.use_ticket(self.ticket.id)

        assert self.ticket.timeActivated is not None
        assert self.ticket.timeActivated > datetime.datetime.now()
        self.tearDown()


    def test_get_ticket_status(self):
        """Tests the get_ticket_status() method of the Ticket class
        
        Creates a new user, creates a new ticket and then
        asserts that the unactivated ticket's status is False. 
        Then, uses the ticket and checks that the activated ticket's status is True.
        Then, sets the ticket's time_activated to an hour ago and checks that the
        ticket's status is False.
        """
        self.setUp()

        self.assertFalse(self.api.get_ticket_status(self.ticket.id))

        self.api.use_ticket(self.ticket.id)
        self.assertFalse(self.api.get_ticket_status(self.ticket.id))

        self.ticket.timeActivated = datetime.datetime.now() - datetime.timedelta(hours=1)
        self.assertTrue(self.api.get_ticket_status(self.ticket.id))

        self.tearDown()

        

    def test_user_buy_ticket(self):
        #Create a test user
        User = ClapITAPI.create_user()
        #Create a test ticket
        Ticket = ClapITAPI.create_ticket(User.id, 350)
        #Assert that the user's tickets is not empty
        assert len(User.tickets) > 0
        
        User.delete()
        Ticket.delete()


    def test_get_favorite_bus_stop(self):
        #Create a test user
        User = ClapITAPI.create_user()
        #Create a test bus stop
        BusStop = ClapITAPI.create_bus_stop()
        #Set the user's favorite bus stop
        User.set_favorite_bus_stop(BusStop)
        #Assert that the user's favorite bus stop is not None
        assert User.get_favorite_bus_stop() is not None
        
        User.delete()
        BusStop.delete()
        