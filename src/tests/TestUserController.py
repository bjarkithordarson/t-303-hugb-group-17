import unittest
from src.models.UserModel import UserModel as User
from src.controllers.UserController import UserController

class TestUserController(unittest.TestCase):
    def setUp(self):
        self.account_controller = UserController()

    def tearDown(self):
        self.account_controller = None

    def test_create_user(self):
        actual = self.account_controller.create_new_user("testboy1", "testman@test.com", "pass123")
        expected = {'username': 'testboy1', 'password': 'pass123', 'email': 'testman@test.com'}
        self.assertEqual(actual, expected)
    def test_delete_user(self):
        actual = self.account_controller.create_new_user("testboy1", "testman@test.com", "pass123")
        expected = None
        self.account_controller.delete_user(actual)
        self.assertEqual(actual, expected)

    def test_update_user_email(self):
        actual = self.account_controller.create_new_user("testboy1", "testman@test.com", "pass123")
        expected = {'username': 'testboy1', 'password': 'pass123', 'email': 'thetester@gmail.com'}
        self.account_controller.update_user_email(actual, "thetester@gmail.com")
        self.assertEqual(actual, expected)
