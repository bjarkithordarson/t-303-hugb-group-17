import unittest
from src.models.TicketModel import TicketModel as Ticket
from src.controllers.TicketController import TicketController

class TestTicketController(unittest.TestCase):
    def setUp(self):
        """Sets up the test case by creating a new TicketController object."""
        self.ticket_controller = TicketController()

    def tearDown(self):
        """Tears down the test case by deleting the TicketController object."""
        self.ticket_controller = None


    def test_get_ticket(self):
        """Tests the get_ticket() method of the TicketController class

        Creates a new user, creates a new ticket, and then checks that the
        ticket's owner is the user, the ticket_price is the same as the one
        passed in, and the time_activated is None.
        """
        self.setUp()
        expected = {"id": 1, "time_activated": "2023-09-25 17:20:36.031604"}
        try:
            actual = self.ticket_controller.get_ticket(1)
        except LookupError:
            actual = None

        self.assertEqual(actual, expected)
        self.tearDown()


    def __test_get_ticket_status(self):
        """Tests the get_ticket_status() method of the TicketController class
        
        Creates a new user, creates a new ticket, and then checks that the
        ticket's owner is the user, the ticket_price is the same as the one
        passed in, and the time_activated is None.
        """
        self.setUp()
        excepted = False
        try:
            ticket = self.test_get_ticket(1)
            actual = self.ticket_controller.get_ticket_status(ticket)
        except LookupError:
            actual = None
        
        self.assertEqual(actual, excepted)
        self.tearDown()

    
