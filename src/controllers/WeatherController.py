import json
from src.weather_service_client_main.client.client import get_current_weather, get_four_day_forecast, get_historical
from google.protobuf.json_format import MessageToJson

class WeatherController:
    '''
    READ THIS!
    https://gitlab.com/grischal/hugb23-weather-service-client/-/tree/main?ref_type=heads
    '''
    def __init__(self):
        # self.API_KEY = 'enter api key'
        # self.SERVICE_URL = 'hugb-service-ep3thvejnq-uc.a.run.app:443'
        pass


    def __get_city_lat_long(self, city_name, country_code):
        '''
        Example:
        __get_city_lat_long("Reykjavik", "IS")

        returns:
        (lat, long)

        Uses the Geocoding API to get the latitude and longitude of a city
        '''
        pass


    def get_current_weather(lat:float = 64.21, lon:float = -21.19):

        try:
            lat = float(lat)
            lon = float(lon)
        except:
            raise ValueError("Latitude and longitude must be floats")

        try:
            weather = get_current_weather(lat=lat, lon=lon)
            weather = json.loads(MessageToJson(weather))
            return weather
        
        except LookupError as err:
            raise LookupError("Weather service error: " + err)


    def get_four_day_forecast(lat:float = 64.21, lon:float = -21.19) -> list[dict]:
        """
        Returns a four-day weather forecast for a given latitude and longitude.

        Args:
        lat (float): The latitude of the location to get the forecast for. Defaults to 64.21.
        lon (float): The longitude of the location to get the forecast for. Defaults to -21.19.

        Returns:
        list: A list of dictionaries containing weather forecast data for the next four days.
        """
        try:
            lat = float(lat)
            lon = float(lon)
        except:
            raise ValueError("Latitude and longitude must be floats")

        try:
            forecast = get_four_day_forecast(lat=lat, lon=lon)
            forecast = json.loads(MessageToJson(forecast))
            return forecast

        except LookupError as err:
            raise LookupError("Weather service error: " + err)


    def get_historical_forecast(city_name, country_code, start_time=None, end_time=None):
        if start_time is None:
            start_time = 1697647873
        if end_time is None:
            end_time = 1697730673

        if not isinstance(start_time, int):
            raise ValueError("Start time must be an integer")
        if not isinstance(end_time, int):
            raise ValueError("End time must be an integer")

        if start_time > end_time:
            raise ValueError("Start time must be before end time")
        
        try:
            forecast = get_historical(location_descriptor=city_name + "," + country_code, start_time=start_time, end_time=end_time)
            forecast = json.loads(MessageToJson(forecast))
            return forecast
        except LookupError as err:
            raise LookupError("Weather service error: " + err)