import datetime

from ..models.TicketModel import TicketModel
from ..models.UserModel import UserModel

class TicketController:
    """The ticket controller class.

    This class is responsible for creating and using tickets."""

    def setup(self):
        """Setup for coverage test"""
        test_ticket = TicketModel()
        assert test_ticket == TicketModel()    
        

    def __init__(self):
        """Initialize the ticket controller."""
        pass

    def create_ticket(self, user):
        """Create a new ticket.
        
        Set the owner of the ticket to a new user, 
        set the time_activated to None and set the ticket_price to 350.
        
        Returns:
        --------
        TicketModel
          The ticket that was created. 
        """
        ticket = TicketModel()
        ticket.owner = user
        ticket.time_activated = None
        ticket.ticket_price = 350
        ticket.save()
        return ticket

    def use_ticket(self, ticket):
        """Use the ticket.
        
        Set the time_activated of the ticket to the current time plus 1.5 hours.
        """
        ticket.time_activated = datetime.datetime.now() + + datetime.timedelta(hours=1.5)
        ticket.save()
        return ticket

    def get_ticket_status(self, ticket):
        """Return the status of the ticket.
        
        Return True if the ticket is activated, False otherwise.
        
        Returns:
        --------
        bool
            The status of the ticket.
        """
        if ticket.time_activated is not None:
            if datetime.datetime.now() > ticket.time_activated:
                return False
            else:
                return True
        else:
            return False



    def get_ticket(self, ticket_id):
        """Return the the ticket.
        
        Return the ticket if found in the database, else raise LookupError
        
        Returns:
        --------
            Ticket
        """
        try:
            ticket = TicketModel.find_by_id(ticket_id)

            return ticket

        except Exception as e:
            raise LookupError(e)

