import uuid
from ..models.BusStopNotificationModel import BusStopNotificationModel as Notification

class BusStopNotificationController:
    def __init__(self):
        self.notification = Notification()

    def create_notification(self, user_id, bus_stop_id, time, status):
        self.notification.id = uuid.uuid4()
        self.notification.user_id = user_id
        self.notification.bus_stop_id = bus_stop_id
        self.notification.time = time
        self.notification.status = status
        self.notification.save()

    def update_notification_status(self, new_status):
        self.notification.status = new_status
        self.notification.save()

    def delete_notification(self):
        self.notification.delete()

