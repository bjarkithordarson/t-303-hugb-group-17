import json
from urllib.request import urlopen
from models.BusModel import BusModel

class BusController:
    ''''''
    def __init__(self):
        self.url = "https://opendata.straeto.is/data/dyn-data/vagnar-all.json"
        res = urlopen(self.url)
        self.data = json.load(res)

    def update_data(self):
        '''Updates the dynamic json file from strætó'''
        res = urlopen(self.url)
        self.data = json.load(res)

    def find_buses_by_route(self, route_id):
        '''Returns buses on the specific route
        
        Parameters:
        ----------
        route_id: str
          the route the user wants to see all the buses on
        Returns:
        ---------
        List of BusModels
        '''
        buses = []
        for leid in self.data["leidir"]:
            if leid['leid'] == route_id:
                for bus in leid["ferlar"]:
                    b = BusModel(bus)
                    buses.append(b)
        
        print("found", len(buses), "buses on route", route_id)
        return buses