import uuid
from ..models.UserModel import UserModel as User
from ..models.TicketModel import TicketModel as Ticket

class UserController:
  def __init__(self):
    """ Constructor for the UserController class"""
    pass


  def create_new_user(self, username, email, password):
      """ Creates a new user and returns the user object
      
      Parameters:
      -----------
      username: str
        The username of the user
      email: str
        The email of the user
      password: str
        The password of the user
      favorite_bus_stops: list
        The list of the user's favourite bus stops
      """
      user = User()
      user.email = email
      user.tickets = []
      user.username = username
      user.password = password
      user.favorite_bus_stops = []

      user.save() 
      return user

    
  def delete_user(self, user):
    """ Deletes the user object from the database
    
    Raises:
    -------
    Exception
      If the user is not in the database.
    """
    user.delete()

  def update_user_email(self, user, new_email):
    """ Updates the user email and saves it to the database
    
    Parameters:
    -----------
    new_email: str
      The new email of the user
      
    Raises:
    -------
    Exception
      If the user is not in the database.
      """
    user.email = new_email
    user.save()

  def get_favourite_bustop(self, user):
    """ Returns the user's favourite bus stop

    Returns:
    --------
    list
      A list of the user's favourite bus stops
    """
    if user.favourite_bus_stops.length < 1 :
      return [] # Returns a empty list
    else:
      return user.favourite_bus_stops


def add_favourite_bustop(self, user, bus_stop):
  """ Adds a bus stop to the user's favourite bus stop list
  
  Parameters:
  -----------
  bus_stop: str
    The bus stop to be added to the user's favourite bus stop list"""
  user.favourite_bus_stops(bus_stop)

def get_user_by_username(self, username):
  u = User.find_by_key("username", username)
  user = u[0] #Return the first one
  if user:
    return user
  else:
    raise Exception("Cannot find user with name '" + username + "'")

def user_login(self, username, password):
    """ Logs the user in and returns the user object

    Returns:
    --------
    UserModel
      The user object"""
    u = get_user_by_username(username)
    if u.password == password:
      return u
    else:
      raise Exception("Login failed for '" + username + "'")
    
def user_buy_ticket(self, user, price):
  """ Creates a new ticket and returns the ticket object

  Parameters:
  -----------
  user: UserModel
    The user object
  price: int
    The price of the ticket

  Returns:
  --------
  TicketModel
    The ticket object"""
  ticket = Ticket()
  ticket.owner = user
  ticket.ticket_price = price
  ticket.save()
  user.tickets.append(ticket)
  user.save()
  return ticket
