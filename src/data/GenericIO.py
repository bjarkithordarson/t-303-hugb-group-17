from __future__ import annotations
import json
import os
import time

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from models.GenericModel import GenericModel

class GenericIO:

  def __init__(self, model):
    self.last_read_date = 0
    self.last_modified_date = 0
    self.current_id = 0
    self.rows = []
    self.model = model
    self.base_dir = "./database/"
    self.filename = f"{type(model).__name__.lower()}.json"
    self.path = os.path.join(self.base_dir, self.filename)
    self.__init_dir()

  def __init_dir(self):
    """Creates the database directory if it does not exist.
    """
    if not os.path.isdir(self.base_dir):
      os.mkdir(self.base_dir)

  def __init_file(self):
    """Creates the database file if it does not exist. """
    with open(self.path, "w+") as file:
        json.dump(self.__get_data_envelope(), file)
        

  def __read_file(self, current_try=0, max_tries=5) -> None:
    """Reads a JSON formatted database file. Initializes the file if it's non-existent or corrupt.

    Args:
        current_try (int, optional): Current try in case of an error. Defaults to 0.
        max_tries (int, optional): Number of initialization tries. Defaults to 5.

    Raises:
        RuntimeError: If database file initialization fails.
    """
    try:
        self.last_modified_date = os.path.getmtime(self.path)
    except FileNotFoundError:
        self.__init_file()

    if self.last_read_date < self.last_modified_date or self.last_read_date == 0:
      try:
        file = open(self.path, "r")
        self.last_read_date = os.path.getmtime(self.path)
        self.__load_file_data(json.load(file))
      except (json.JSONDecodeError, FileNotFoundError):
        self.__init_file()
        if current_try < max_tries:
          time.sleep(current_try)
          self.__read_file(current_try + 1, max_tries)
        else:
          raise RuntimeError(f"Unable to initialize database file '{self.path}'.")

  def __write_file(self) -> None:
    """Writes the data to the database file.
    """
    try:
      self.last_modified_date = os.path.getmtime(self.path)
      with open(self.path, "w") as file:
        json.dump(self.__get_data_envelope(), file)
    except Exception as e:
      print(e)
    
  def __load_file_data(self, file_data:dict) -> None:
    """Parses JSON and populates metadata and rows.

    Args:
        file_data (dict): The data to be parsed.
    """
    self.current_id = file_data["meta"]["current_id"]
    self.rows = file_data["rows"]

  def __get_data_envelope(self) -> dict:
    """Wraps the current data inside an envelope for writing to database file.

    Returns:
        dict: Enveloped data.
    """
    return {
      "meta": {
        "current_id": self.current_id
      },
      "rows": [row for row in self.rows]
    }

  def __init_file(self):
    with open(self.path, "w+") as file:
      json.dump(self.__get_data_envelope(), file)

  def __generate_id(self) -> int:
    """Returns an incremented id.

    Returns:
        int: id
    """
    return self.current_id + 1

  def __type_cast(self, data:dict) -> GenericModel:
    """Converts a dictionary into a model.

    Args:
        data (dict): The dictionary to be converted.

    Returns:
        Model: The model containing the dictionary data.
    """
    return type(self.model)(data)
    
  def find_by_key(self, key:str, value:any) -> list:
    """Returns all rows that match the given key and value.

    Args:
        key (str): The key to filter by.
        value (any): The value to filter by.

    Returns:
        list: A list of models.
    """
    self.__read_file()
    return [self.__type_cast(row) for row in self.rows if key in row and row[key] == value]
    

  def find_all(self) -> list:
    """Returns all rows.

    Returns:
        list: A list of models
    """
    self.__read_file()
    return [self.__type_cast(row) for row in self.rows]

  def insert(self, entity:GenericModel) -> GenericModel:
    """Inserts a model into the database.

    Args:
        entity (Model): The model to be inserted.

    Returns:
        Model: The model with an added ID.
    """
    self.__read_file()
    self.current_id = self.__generate_id()
    setattr(entity, 'id', self.current_id)
    self.rows.append(entity.data)
    self.__write_file()
    return entity

  def update(self, entity) -> GenericModel:
    """Updates the database with the given model. The model MUST include an ID.

    Args:
        entity (Model): The model to be inserted.

    Returns:
        Model: The updated model.
    """
    self.__read_file()
    entities = self.find_by_key("id", entity.data["id"])
    for index, row in enumerate(self.rows):
      if row["id"] == entity.id:
        self.rows[index] = entity.data
    self.__write_file()

    return entity


  def delete(self, id:int) -> bool:
    """Deletes a row from the database.

    Args:
        id (int): The ID of the row to be deleted.

    Raises:
        NotImplemented: _description_

    Returns:
        bool: True if the row was deleted, else false.
    """
    try:
      self.__read_file()
      self.rows = [row for row in self.rows if row["id"] != id]
      self.__write_file()
      return True
    except Exception as e:
      raise NotImplemented("TODO")