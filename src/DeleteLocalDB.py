import models.UserModel as User
import models.TicketModel as Ticket

class DeleteLocalDB:
    def __init__(self):
        """ Constructor for the DeleteLocalDB class"""
        self.user =  User.UserModel() 
        self.ticket = Ticket.TicketModel()

    def delete_user(self):
        """ Deletes the user object from the database"""
        for user in self.user.find_all():
            user.delete()

    def delete_ticket(self):
        """ Deletes the ticket object from the database"""
        for ticket in self.ticket.find_all():
            ticket.delete()
