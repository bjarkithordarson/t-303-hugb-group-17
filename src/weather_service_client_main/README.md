## Sample Client

This is the sample client application for the deployed weather service. You will find details in the source files, as well as in the technical description of the service for Sprint 4: <https://reykjavik.instructure.com/courses/7338/assignments/72748>.

To start the client, run ``python -m client.client`` from the top-level folder. To re-generate the grpc helper files, run ``python -m grpc_tools.protoc -I . --python_out=./grpc_helpers --grpc_python_out=./grpc_helpers ./proto/weatherStation.proto``, then fix the import of ``weatherStation_pb2`` in ``grpc_helpers/weatherStation_pb2_grpc.py``.