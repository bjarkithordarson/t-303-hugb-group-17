import grpc
from src.weather_service_client_main.grpc_helpers import weatherStation_pb2
from src.weather_service_client_main.grpc_helpers import weatherStation_pb2_grpc

API_KEY = '21c20c51-406c-44db-82f6-524172130dd2'
SERVICE_URL = 'hugb-service-ep3thvejnq-uc.a.run.app:443'


def get_current_weather(lat:int = 64.21, lon:int = -21.19) -> list:
   """
   Retrieves the current weather for a given location using the WeatherStation service.

   Args:
      lat (float): The latitude of the location. Defaults to 64.21.
      lon (float): The longitude of the location. Defaults to -21.19.

   Returns:
      A list containing the current weather information for the given location.
   """
   try:
      with grpc.secure_channel(SERVICE_URL, grpc.ssl_channel_credentials()) as channel:
         #This is just as before: We import the grpc definitions.
         stub = weatherStation_pb2_grpc.WeatherStationStub(channel)

         #Call the three different methods. For parameter and return type descritions, see the proto file
         current_weather = stub.get_current_weather(weatherStation_pb2.Location(lat=lat, lon=lon, api_key=API_KEY))
   except grpc.RpcError as e:
      raise LookupError(e.details())

   else:
      return current_weather


def get_four_day_forecast(lat:float = 64.21, lon:float=-21.19) -> list:
   """
   Retrieves a four-day weather forecast for a given location.

   Args:
      lat (float): The latitude of the location. Defaults to 64.21.
      lon (float): The longitude of the location. Defaults to -21.19.

   Returns:
      The return value is a list with 96 elements, 
      each of which contains the weather for a specified hour.
   """
   try:
      with grpc.secure_channel(SERVICE_URL, grpc.ssl_channel_credentials()) as channel:
         stub = weatherStation_pb2_grpc.WeatherStationStub(channel)
         forecast = stub.get_four_day_forecast(weatherStation_pb2.Location(lat=lat, lon=lon, api_key=API_KEY))

         return forecast
   except grpc.RpcError as e:
      raise LookupError(e.details())

   

def get_historical(location_descriptor:str = "Reykjavik,IS", start_time:int = 1697647873, end_time:int = 1697730673) -> list:
   """
      Retrieves historical weather forecast data for a given location and time range.

      Args:
         location_descriptor (str): A string describing the location to retrieve weather data for.
         start_time (int): The start time of the time range to retrieve weather data for, in Unix timestamp format.
         end_time (int): The end time of the time range to retrieve weather data for, in Unix timestamp format.
         
         The time range must be less than 24 hours and the end time must be after the start time.
      Returns:
         A list of weather forecast data for the specified location and time range.
   """
   try:
      #Here is a difference to the client we have used so far: We are using a secure channel.
      with grpc.secure_channel(SERVICE_URL, grpc.ssl_channel_credentials()) as channel:

         #This is just as before: We import the grpc definitions.
         stub = weatherStation_pb2_grpc.WeatherStationStub(channel)

         historical = stub.get_historical_forecast(weatherStation_pb2.HistoryPlace(location_descriptor=location_descriptor, start_time=start_time, end_time=end_time, api_key=API_KEY))

   #Error handling in case the grpc connection throws an error. This is how the service provides errors.
   except grpc.RpcError as e:
      raise LookupError(e.details())

   else:

      return historical.forecast


# def run():
#    #Replace this with the api key you received via email
#    try:
#       #Here is a difference to the client we have used so far: We are using a secure channel.
#       with grpc.secure_channel(SERVICE_URL, grpc.ssl_channel_credentials()) as channel:

#          #This is just as before: We import the grpc definitions.
#          stub = weatherStation_pb2_grpc.WeatherStationStub(channel)

#          #Call the three different methods. For parameter and return type descritions, see the proto file
#          current_weather = stub.get_current_weather(weatherStation_pb2.Location(lat=64.21, lon=-21.19, api_key=API_KEY))
#          forecast = stub.get_four_day_forecast(weatherStation_pb2.Location(lat=64.21, lon=-21.19, api_key=API_KEY))
#          historical = stub.get_historical_forecast(weatherStation_pb2.HistoryPlace(location_descriptor="Reykjavik,IS", start_time=1697647873, end_time=1697730673, api_key=API_KEY))

#    #Error handling in case the grpc connection throws an error. This is how the service provides errors.
#    except grpc.RpcError as e:
#       # print gRPC error message
#       print(e.details())
#       status_code = e.code()
#       # should print `INVALID_ARGUMENT`
#       print(status_code.name)
#       # should print `(3, 'invalid argument')`
#       print(status_code.value)
#    else:
#       #If no error, print the return values of the three different calls.
#       print("Current weather: " , current_weather)
#       print("4-day forecast: " , forecast.forecast)
#       print("Historical forecast: " , historical.forecast)

# run()