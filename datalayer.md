# How to define models

A model should extend `GenericModel`. Fields are defined in the model.

## Example
```python
from models.GenericModel import GenericModel

class DemoModel(GenericModel):
    @property
    def id(self):
        """A simple field getter."""
        return DemoModel.generic_getter(self, "id")

    # Note that to be able to set the field value, we need a setter as well.
    @id.setter
    def id(self, value):
        return DemoModel.generic_setter(self, "id", value)

    @property
    def title(self):
        """A simple field getter."""
        return DemoModel.generic_getter(self, "title")

    @title.setter
    def title(self, value):
        return DemoModel.generic_setter(self, "title", value)

    # Most fields can use the generic_getter and generic_setter unless you need
    # relations.

    @property
    def author(self):
        return DemoModel.one_to_one_getter(self, "author")

    @author.setter
    def author(self, value):
        return DemoModel.one_to_one_setter(self, value, "author")

    # If you want many-to-many relations, use many_to_many_getter and many_to_many_setter

    @property
    def tags(self):
        return DemoModel.many_to_many_getter(self, "tags")

    @tags.setter
    def tags(self, value):
        return TestModel.many_to_many_setter(self, value, "tags")
    
```

## How to use the model?
### Create
```python
demo = DemoModel()
demo.title = "This is a title!"
demo.author = AuthorModel.find_by_id(1) # A field with one-to-one relations
demo.tags = [TagsModel.find_by_id(1), TagsModel.find_by_id(2)] # Many-to-many relations
demo.save() # Saves the model to a file
```

### Find by ID
```python
demo = DemoModel.find_by_id(1)
```

### Find by key
```python
demos = DemoModel.find_by_key("title", "This is a title!")
```

### Find all
```python
demos = DemoModel.find_all()
```

### Using properties
```python
demo = DemoModel.find_by_id(1)
print(demo.title) # Prints "This is a title!"
print(demo.author.name) # Prints the author's (one-to-one) name from a hypothetical AuthorModel
print(demo.tags[0].tag) # Prints the first tag (many-to-many) from a hypothetical TagModel
```

### Find and update
```python
demo = DemoModel.find_by_id(1)
demo.title = "This is a new title!"
demo.save()
```
